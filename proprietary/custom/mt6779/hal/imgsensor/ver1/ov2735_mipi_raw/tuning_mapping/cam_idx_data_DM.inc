#define IDX_DATA_DM_DIM_NS    4
#define IDX_DATA_DM_FACTOR_SZ    3
#define IDX_DATA_DM_ENTRY_NS    43

static unsigned int _cam_data_entry_DM_key0000[] = {0X0000000C, 0XA0000000, 0X00001F00, };
static unsigned int _cam_data_entry_DM_key0001[] = {0X00000020, 0X80000000, 0X00001F00, };
static unsigned int _cam_data_entry_DM_key0002[] = {0X00000000, 0X80100000, 0X00001F00, };
static unsigned int _cam_data_entry_DM_key0003[] = {0X00000000, 0X80400000, 0X00001F00, };
static unsigned int _cam_data_entry_DM_key0004[] = {0X0000000C, 0X60000000, 0X00001F00, };
static unsigned int _cam_data_entry_DM_key0005[] = {0X00000020, 0X40000000, 0X00001F00, };
static unsigned int _cam_data_entry_DM_key0006[] = {0X00000000, 0X40100000, 0X00001F00, };
static unsigned int _cam_data_entry_DM_key0007[] = {0X00000000, 0X40400000, 0X00001F00, };
static unsigned int _cam_data_entry_DM_key0008[] = {0X0000000C, 0XA0000000, 0X00002F00, };
static unsigned int _cam_data_entry_DM_key0009[] = {0X00000020, 0X80000000, 0X00002F00, };
static unsigned int _cam_data_entry_DM_key0010[] = {0X00000000, 0X80100000, 0X00002F00, };
static unsigned int _cam_data_entry_DM_key0011[] = {0X00000000, 0X80400000, 0X00002F00, };
static unsigned int _cam_data_entry_DM_key0012[] = {0X0000000C, 0X60000000, 0X00002F00, };
static unsigned int _cam_data_entry_DM_key0013[] = {0X00000020, 0X40000000, 0X00002F00, };
static unsigned int _cam_data_entry_DM_key0014[] = {0X00000000, 0X40100000, 0X00002F00, };
static unsigned int _cam_data_entry_DM_key0015[] = {0X00000000, 0X40400000, 0X00002F00, };
static unsigned int _cam_data_entry_DM_key0016[] = {0X00000001, 0X40040000, 0X00001F00, };
static unsigned int _cam_data_entry_DM_key0017[] = {0X00000001, 0X80040000, 0X00001100, };
static unsigned int _cam_data_entry_DM_key0018[] = {0X00000001, 0X00040000, 0X00001F01, };
static unsigned int _cam_data_entry_DM_key0019[] = {0X00000001, 0X40040000, 0X00002F00, };
static unsigned int _cam_data_entry_DM_key0020[] = {0X00000001, 0X80040000, 0X00002100, };
static unsigned int _cam_data_entry_DM_key0021[] = {0X00000001, 0X00040000, 0X00002F01, };
static unsigned int _cam_data_entry_DM_key0022[] = {0X00000002, 0X40080000, 0X00003F00, };
static unsigned int _cam_data_entry_DM_key0023[] = {0X00000002, 0X00080000, 0X00003F01, };
static unsigned int _cam_data_entry_DM_key0024[] = {0X00000000, 0XC0000330, 0X00003FFF, };
static unsigned int _cam_data_entry_DM_key0025[] = {0X00000000, 0XC0000CC0, 0X00003FFF, };
static unsigned int _cam_data_entry_DM_key0026[] = {0X00000000, 0XC001B000, 0X00003FFF, };
static unsigned int _cam_data_entry_DM_key0027[] = {0X00CC0000, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_DM_key0028[] = {0X03300000, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_DM_key0029[] = {0X6C000000, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_DM_key0030[] = {0X00000000, 0XC0000001, 0X00003FFF, };
static unsigned int _cam_data_entry_DM_key0031[] = {0X00000600, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_DM_key0032[] = {0X00001800, 0XC0000000, 0X00003FFF, };
static unsigned int _cam_data_entry_DM_key0033[] = {0X0001E000, 0XC0000000, 0X000031FF, };
static unsigned int _cam_data_entry_DM_key0034[] = {0X0001E00C, 0XE0000000, 0X000036FF, };
static unsigned int _cam_data_entry_DM_key0035[] = {0X00000020, 0XC0000000, 0X000036FF, };
static unsigned int _cam_data_entry_DM_key0036[] = {0X00000000, 0XC0100000, 0X000036FF, };
static unsigned int _cam_data_entry_DM_key0037[] = {0X00000000, 0XC0400000, 0X000036FF, };
static unsigned int _cam_data_entry_DM_key0038[] = {0X6C000000, 0XC001B000, 0X000036FF, };
static unsigned int _cam_data_entry_DM_key0039[] = {0X00000000, 0XC0000001, 0X000036FF, };
static unsigned int _cam_data_entry_DM_key0040[] = {0X00000001, 0X80040000, 0X00001600, };
static unsigned int _cam_data_entry_DM_key0041[] = {0X00000001, 0X80040000, 0X00002600, };
static unsigned int _cam_data_entry_DM_key0042[] = {0X00000000, 0XC3800000, 0X00003FFF, };

static IDX_MASK_ENTRY _cam_data_entry_DM[IDX_DATA_DM_ENTRY_NS] =
{
    {(unsigned int*)&_cam_data_entry_DM_key0000, 0, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0001, 8, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0002, 16, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0003, 24, 0, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0004, 0, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0005, 8, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0006, 16, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0007, 24, 1, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0008, 32, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0009, 40, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0010, 16, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0011, 24, 2, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0012, 32, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0013, 40, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0014, 16, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0015, 24, 3, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0016, 48, 4, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0017, 48, 5, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0018, 48, 6, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0019, 48, 7, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0020, 48, 8, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0021, 48, 9, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0022, 56, 10, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0023, 56, 11, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0024, 48, 12, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0025, 56, 13, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0026, 0, 14, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0027, 48, 15, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0028, 56, 16, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0029, 0, 17, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0030, 8, 17, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0031, 48, 18, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0032, 56, 19, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0033, 0, 20, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0034, 0, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0035, 8, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0036, 16, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0037, 24, 21, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0038, 0, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0039, 8, 22, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0040, 48, 23, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0041, 48, 24, 0, 0},
    {(unsigned int*)&_cam_data_entry_DM_key0042, 64, 25, 0, 0},
};

static unsigned short _cam_data_dims_DM[] = 
{
    EDim_IspProfile,
    EDim_SensorMode,
    EDim_Flash,
    EDim_FaceDetection,
};

static unsigned short _cam_data_expand_DM[] = 
{0, 0, 1};

const IDX_MASK_T cam_data_DM =
{
    {IDX_ALGO_MASK, IDX_DATA_DM_DIM_NS, (unsigned short*)&_cam_data_dims_DM, (unsigned short*)&_cam_data_expand_DM},
    {IDX_DATA_DM_ENTRY_NS, IDX_DATA_DM_FACTOR_SZ, (IDX_MASK_ENTRY*)&_cam_data_entry_DM}
};